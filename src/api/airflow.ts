import axios from "axios";
import { AxiosInstance } from "axios";
import applyCaseMiddleware from "axios-case-converter";

export enum DagRunState {
  SUCCESS = "success",
  RUNNING = "running",
  FAILED = "failed",
}

export interface CreateLineupConf {
  modelId?: number;
  contestId: number;
}

export interface TriggerDagRunRequest {
  dagRunId?: string;
  executionDate: string;
  conf: CreateLineupConf;
}

export interface Dag {
  dagId: string;
}

export interface ListDagsResponse {
  dags: Array<Dag>;
  totalEntries: number;
}

class Airflow {
  service: AxiosInstance;
  constructor() {
    this.service = applyCaseMiddleware(
      axios.create({
        baseURL: "/airflow/api/v1",
        auth: {
          username: "admin",
          password: "admin",
        },
      }),
    );
  }

  async triggerDagRun(dagId: string, request: TriggerDagRunRequest) {
    await this.service.post(`/dags/${dagId}/dagRuns`, request);
  }

  async listDags(): Promise<ListDagsResponse> {
    const resp = await this.service.get<ListDagsResponse>(`/dags`);
    return resp.data;
  }
}

export default new Airflow();
