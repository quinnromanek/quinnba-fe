import { AxiosInstance } from "axios";
import axios from "axios";
import Configuration from "@/configuration";
import { ListContestsResponse } from "@/models/contests";

class Quinnba {
  service: AxiosInstance;
  constructor() {
    this.service = axios.create({
      baseURL: Configuration.value("quinnbaUrl"),
    });
  }

  async listContests(): Promise<ListContestsResponse> {
    const resp = await this.service.get<ListContestsResponse>("/contests");
    return resp.data;
  }
}

export default new Quinnba();
