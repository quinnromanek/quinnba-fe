import { Module } from "vuex";
import { RootState, GamesState } from "@/store/types";
import axios from "axios";
import {
  DatesResponse,
  ResultsResponse,
  LineupsResponse,
  Lineup,
} from "@/models/games";
import Airflow from "@/api/airflow";
import { TriggerDagRunRequest, CreateLineupConf } from "@/api/airflow";
import Configuration from "@/configuration";

interface CreateLineupArgs {
  date: string;
  conf: CreateLineupConf;
}

export const games: Module<GamesState, RootState> = {
  namespaced: true,
  state: {
    allowedDates: [],
    date: new Date().toISOString().substr(0, 10),
    results: [],
    lineups: [],
  },
  mutations: {
    setAllowedDates(state: GamesState, dates: Array<string>) {
      state.allowedDates = dates;
    },
    setResults(state: GamesState, response: ResultsResponse) {
      state.date = response.date;
      state.results = response.players;
    },
    setLineups(state: GamesState, lineups: Array<Lineup>) {
      state.lineups = lineups;
    },
  },
  actions: {
    async loadAllowedDates({ commit }) {
      try {
        const dates = await axios.get<DatesResponse>(
          Configuration.value("quinnbaUrl") + "/games/dates",
        );
        commit("setAllowedDates", dates.data.dates);
      } catch (e) {
        commit("setError", e.toString(), { root: true });
      }
    },
    async loadResults({ commit }, { date }) {
      try {
        let url = Configuration.value("quinnbaUrl") + "/games/results";
        if (date) {
          url += `/${date}`;
        }

        const resp = await axios.get<ResultsResponse>(url);
        commit("setResults", resp.data);
      } catch (e) {
        commit("setError", e.toString(), { root: true });
      }
    },
    async loadLineups({ commit }, { date }) {
      try {
        let url = Configuration.value("quinnbaUrl") + "/games/lineups";
        if (date) {
          url += `/${date}`;
        }

        const resp = await axios.get<LineupsResponse>(url);
        commit("setLineups", resp.data.lineups);
      } catch (e) {
        commit("setError", e.toString(), { root: true });
      }
    },

    async updateActive({ dispatch, commit }, { date, update }) {
      try {
        const url =
          Configuration.value("quinnbaUrl") + "/games/results/" + date;
        await axios.post(url, update);
        await Promise.all([
          dispatch("loadResults", { date }),
          dispatch("loadLineups", { date }),
        ]);
      } catch (e) {
        commit("setError", e.toString(), { root: true });
      }
    },

    async createLineup({ commit }, { date, conf }: CreateLineupArgs) {
      const now = new Date();
      const executionDate = new Date(date);
      // We only care about what date it runs on, but executionDate
      // must be unique for all DAG runs.
      executionDate.setMinutes(now.getMinutes());
      executionDate.setSeconds(now.getSeconds());

      const request: TriggerDagRunRequest = {
        executionDate: executionDate.toISOString(),
        conf: conf,
      };
      try {
        await Airflow.triggerDagRun("create_lineup", request);
      } catch (e) {
        commit("setError", e.toString(), { root: true });
      }
    },
  },
};
