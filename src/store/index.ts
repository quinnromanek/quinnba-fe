import axios from "axios";
import Vue from "vue";
import Vuex, { StoreOptions } from "vuex";
import VueAxios from "vue-axios";
import {
  MissingPlayersResponse,
  PlayersResponse,
  Player,
} from "@/models/players";
import { BalanceResponse, Season } from "@/models/home";
import Configuration from "@/configuration";
import { RootState } from "@/store/types";
import { games } from "@/store/games";
import { models } from "@/store/models";
import { contests } from "@/store/contests";
import { apolloClient } from "@/vue-apollo";
import { MISSING_PLAYERS_QUERY } from "@/graphql/players.queries";
import {
  MissingPlayersQuery,
  MissingPlayersQueryVariables,
} from "@/generated/graphql";

Vue.use(Vuex);
Vue.use(VueAxios, axios);

const store: StoreOptions<RootState> = {
  state: {
    missing: [],
    matching: [],
    error: "",
    seasons: [],
  },
  getters: {
    isError: (state: RootState) => state.error.length > 0,
  },
  mutations: {
    setMissing(state: RootState, missing: RootState["missing"]) {
      state.missing = missing;
    },
    setMatching(state: RootState, matching: Array<Player>) {
      state.matching = matching;
    },
    setError(state: RootState, error: string) {
      state.error = error;
    },
    setSeasons(state: RootState, season: Array<Season>) {
      state.seasons = season;
    },
  },
  actions: {
    async loadMissing({ commit }) {
      try {
        const { data } = await apolloClient.query<
          MissingPlayersQuery,
          MissingPlayersQueryVariables
        >({
          query: MISSING_PLAYERS_QUERY,
        });
        commit("setMissing", data.missingPlayers);
      } catch (e) {
        commit("setError", e.toString());
      }
    },

    async loadMatching({ commit }) {
      try {
        const matching = await axios.get<PlayersResponse>(
          Configuration.value("quinnbaUrl") + "/players/missing_nbaid",
        );
        commit("setMatching", matching.data.missing);
      } catch (e) {
        commit("setError", e.toString());
      }
    },
    async loadSeasons({ commit }) {
      try {
        const matching = await axios.get<BalanceResponse>(
          Configuration.value("quinnbaUrl") + "/balance",
        );
        commit("setSeasons", matching.data.seasons);
      } catch (e) {
        commit("setError", e.toString());
      }
    },

    async matchPlayers({ dispatch, commit }, { gid, nbaid }) {
      try {
        await axios.post(
          Configuration.value("quinnbaUrl") + "/players/missing_gid",
          {
            gid: gid,
            nbaid: nbaid,
          },
        );
        await Promise.all([dispatch("loadMissing"), dispatch("loadMatching")]);
      } catch (e) {
        commit("setError", e.toString());
      }
    },

    async clearError({ commit }) {
      commit("setError", "");
    },
  },
  modules: {
    games,
    models,
    contests,
  },
};

export default new Vuex.Store<RootState>(store);
