import { Player } from "@/models/players";
import { PlayerResults, Lineup } from "@/models/games";
import { ModelInfo } from "@/models/models";
import { Contest } from "@/models/contests";
import { Season } from "@/models/home";
import { MissingPlayersQuery } from "@/generated/graphql";

export interface RootState {
  missing: MissingPlayersQuery["missingPlayers"];
  matching: Array<Player>;
  error: string;
  seasons: Array<Season>;
}

export interface GamesState {
  allowedDates: Array<string>;
  date: string;
  results: Array<PlayerResults>;
  lineups: Array<Lineup>;
}

export interface ModelsState {
  models: Array<ModelInfo>;
}

export interface ContestsState {
  contests: Array<Contest>;
}
