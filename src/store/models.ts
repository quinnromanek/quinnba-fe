import { Module } from "vuex";
import { RootState, ModelsState } from "@/store/types";
import axios from "axios";
import {
  ModelsResponse,
  SetActiveModelRequest,
  ModelInfo,
} from "@/models/models";
import Configuration from "@/configuration";

export const models: Module<ModelsState, RootState> = {
  namespaced: true,
  state: {
    models: [],
  },
  mutations: {
    setModels(state: ModelsState, models: Array<ModelInfo>) {
      state.models = models;
    },
  },
  actions: {
    async loadModels({ commit }) {
      try {
        const url = Configuration.value("quinnbaUrl") + "/models";
        const resp = await axios.get<ModelsResponse>(url);
        commit("setModels", resp.data.models);
      } catch (e) {
        commit("setError", e.toString(), { root: true });
      }
    },
  },
};
