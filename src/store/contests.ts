import { Module } from "vuex";
import { RootState, ContestsState } from "@/store/types";
import axios from "axios";
import { Contest } from "@/models/contests";
import Quinnba from "@/api/quinnba";

export const contests: Module<ContestsState, RootState> = {
  namespaced: true,
  state: {
    contests: [],
  },
  mutations: {
    setContests(state: ContestsState, contests: Array<Contest>) {
      state.contests = contests;
    },
  },
  actions: {
    async loadContests({ commit }) {
      try {
        const resp = await Quinnba.listContests();
        commit("setContests", resp.contests);
      } catch (e) {
        commit("setError", e.toString(), { root: true });
      }
    },
  },
};
