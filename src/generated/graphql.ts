export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /**
   * The `Date` scalar type represents a Date
   * value as specified by
   * [iso8601](https://en.wikipedia.org/wiki/ISO_8601).
   */
  Date: any;
};

export type CorrectMissingPlayer = {
  __typename?: "CorrectMissingPlayer";
  player: Player;
};

export enum CorrectableField {
  FanduelId = "fanduel_id",
  Nbaid = "nbaid",
}

export type FanduelPlayer = {
  fanduelId: Scalars["ID"];
  injuryStatus?: InputMaybe<Scalars["String"]>;
  opp: Team;
  playerName: Scalars["String"];
  pos: Position;
  salary: Scalars["Int"];
  secondaryPos?: InputMaybe<Position>;
  team: Team;
};

export type IngestSalaries = {
  __typename?: "IngestSalaries";
  count?: Maybe<Scalars["Int"]>;
};

export type IngestStats = {
  __typename?: "IngestStats";
  count?: Maybe<Scalars["Int"]>;
};

export type Mutations = {
  __typename?: "Mutations";
  correctMissingPlayer?: Maybe<CorrectMissingPlayer>;
  ingestNbaStats?: Maybe<IngestStats>;
  ingestSalaries?: Maybe<IngestSalaries>;
  notifyDiscord?: Maybe<NotifyDiscord>;
  promoteMissingPlayer?: Maybe<PromoteMissingPlayer>;
};

export type MutationsCorrectMissingPlayerArgs = {
  field: CorrectableField;
  sourceId: Scalars["ID"];
  targetId: Scalars["ID"];
};

export type MutationsIngestNbaStatsArgs = {
  stats: Array<InputMaybe<NbaStatLine>>;
};

export type MutationsIngestSalariesArgs = {
  players: Array<InputMaybe<FanduelPlayer>>;
};

export type MutationsNotifyDiscordArgs = {
  message: Scalars["String"];
  source: Scalars["String"];
};

export type MutationsPromoteMissingPlayerArgs = {
  playerIds: Array<InputMaybe<Scalars["ID"]>>;
};

export type NbaStatLine = {
  ast: Scalars["Float"];
  blk: Scalars["Float"];
  date: Scalars["Date"];
  dreb: Scalars["Float"];
  fg3a: Scalars["Float"];
  fg3m: Scalars["Float"];
  fga: Scalars["Float"];
  fgm: Scalars["Float"];
  fta: Scalars["Float"];
  ftm: Scalars["Float"];
  gameId: Scalars["ID"];
  inj: Scalars["Boolean"];
  min: Scalars["Float"];
  opp: Team;
  oreb: Scalars["Float"];
  out: Scalars["Boolean"];
  pctAst: Scalars["Float"];
  pctBlk: Scalars["Float"];
  pctBlka: Scalars["Float"];
  pctDreb: Scalars["Float"];
  pctFg3a: Scalars["Float"];
  pctFg3m: Scalars["Float"];
  pctFga: Scalars["Float"];
  pctFgm: Scalars["Float"];
  pctFta: Scalars["Float"];
  pctFtm: Scalars["Float"];
  pctOreb: Scalars["Float"];
  pctPf: Scalars["Float"];
  pctPfd: Scalars["Float"];
  pctPts: Scalars["Float"];
  pctReb: Scalars["Float"];
  pctStl: Scalars["Float"];
  pctTov: Scalars["Float"];
  pf: Scalars["Float"];
  playerId: Scalars["ID"];
  playerName: Scalars["String"];
  pts: Scalars["Float"];
  reb: Scalars["Float"];
  start: Scalars["Boolean"];
  stl: Scalars["Float"];
  team: Team;
  to: Scalars["Float"];
  usgPct: Scalars["Float"];
};

/** An object with an ID */
export type Node = {
  /** The ID of the object. */
  id: Scalars["ID"];
};

export type NotifyDiscord = {
  __typename?: "NotifyDiscord";
  message?: Maybe<Scalars["String"]>;
};

/** The Relay compliant `PageInfo` type, containing data necessary to paginate this connection. */
export type PageInfo = {
  __typename?: "PageInfo";
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars["String"]>;
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars["Boolean"];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars["Boolean"];
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars["String"]>;
};

export type Player = Node & {
  __typename?: "Player";
  fanduelId?: Maybe<Scalars["Int"]>;
  first?: Maybe<Scalars["String"]>;
  gid: Scalars["ID"];
  /** The ID of the object. */
  id: Scalars["ID"];
  last?: Maybe<Scalars["String"]>;
  missing?: Maybe<Scalars["Boolean"]>;
  name?: Maybe<Scalars["String"]>;
  nbaid?: Maybe<Scalars["Int"]>;
  nerdid?: Maybe<Scalars["Int"]>;
};

export type PlayerConnection = {
  __typename?: "PlayerConnection";
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<PlayerEdge>>;
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
};

/** A Relay edge containing a `Player` and its cursor. */
export type PlayerEdge = {
  __typename?: "PlayerEdge";
  /** A cursor for use in pagination */
  cursor: Scalars["String"];
  /** The item at the end of the edge */
  node?: Maybe<Player>;
};

/** An enumeration. */
export enum Position {
  C = "C",
  Pf = "PF",
  Pg = "PG",
  Sf = "SF",
  Sg = "SG",
}

export type PromoteMissingPlayer = {
  __typename?: "PromoteMissingPlayer";
  players: Array<Maybe<Player>>;
};

export type Query = {
  __typename?: "Query";
  allPlayers?: Maybe<PlayerConnection>;
  missingGames: Array<Maybe<Scalars["Date"]>>;
  missingPlayers: Array<Maybe<Player>>;
  /** The ID of the object */
  node?: Maybe<Node>;
  trainingInput: Array<Maybe<TrainingRow>>;
};

export type QueryAllPlayersArgs = {
  after?: InputMaybe<Scalars["String"]>;
  before?: InputMaybe<Scalars["String"]>;
  first?: InputMaybe<Scalars["Int"]>;
  last?: InputMaybe<Scalars["Int"]>;
};

export type QueryNodeArgs = {
  id: Scalars["ID"];
};

export type QueryTrainingInputArgs = {
  season: Scalars["Int"];
};

/** An enumeration. */
export enum Team {
  Atl = "atl",
  Bkn = "bkn",
  Bos = "bos",
  Cha = "cha",
  Chi = "chi",
  Cle = "cle",
  Dal = "dal",
  Den = "den",
  Det = "det",
  Gsw = "gsw",
  Hou = "hou",
  Ind = "ind",
  Lac = "lac",
  Lal = "lal",
  Mem = "mem",
  Mia = "mia",
  Mil = "mil",
  Min = "min",
  Nop = "nop",
  Nyk = "nyk",
  Okc = "okc",
  Orl = "orl",
  Phi = "phi",
  Phx = "phx",
  Por = "por",
  Sac = "sac",
  Sas = "sas",
  Tor = "tor",
  Uta = "uta",
  Was = "was",
}

export type TrainingRow = {
  __typename?: "TrainingRow";
  avgAst: Scalars["Float"];
  avgBlk: Scalars["Float"];
  avgMin: Scalars["Float"];
  avgPts: Scalars["Float"];
  avgReb: Scalars["Float"];
  avgStl: Scalars["Float"];
  avgTo: Scalars["Float"];
  fpts: Scalars["Float"];
};

export type MissingPlayersQueryVariables = Exact<{ [key: string]: never }>;

export type MissingPlayersQuery = {
  __typename?: "Query";
  missingPlayers: Array<
    | {
        __typename?: "Player";
        gid: string;
        name?: string | null | undefined;
        nbaid?: number | null | undefined;
        fanduelId?: number | null | undefined;
      }
    | null
    | undefined
  >;
};

export interface PossibleTypesResultData {
  possibleTypes: {
    [key: string]: string[];
  };
}
const result: PossibleTypesResultData = {
  possibleTypes: {
    Node: ["Player"],
  },
};
export default result;
