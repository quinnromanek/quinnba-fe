export interface ModelsResponse {
  models: Array<ModelInfo>;
}

export interface ModelStats {
  season?: number;
  count: number;
  skew: number;
  kurtosis: number;
  rmse: number;
  me: number;
}

export interface ModelInfo {
  id: number;
  active: boolean;
  stats: ModelStats;
  seasons: Array<ModelStats>;
}

export interface SetActiveModelRequest {
  model: number;
}
