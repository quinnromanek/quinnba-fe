export interface ResultsResponse {
  date: string;
  players: Array<PlayerResults>;
}

export interface PlayerResults {
  active: boolean;
  gid: number;
  name: string;
  team: string;
  pos: string;
  salary: number;
  pred: number;
  fpts: number;
  min: number;
  pts: number;
  ast: number;
  reb: number;
  blk: number;
  stl: number;
  to: number;
}

export interface DatesResponse {
  dates: Array<string>;
}

export interface LineupEntry {
  gid: number;
  name: string;
  pos: string;
  salary: number;
  pred: number;
  fpts?: number;
}

export interface LineupContest {
  id: number;
  name: string;
  cost: number;
  entries: number;
}

export interface Lineup {
  id: number;
  expected: number;
  mid: number;
  actual?: number;
  winnings: number;
  contest: LineupContest;
  players: Array<LineupEntry>;
}

export interface LineupsResponse {
  lineups: Array<Lineup>;
}
