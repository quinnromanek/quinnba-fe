export interface MissingPlayersResponse {
  missing: Array<MissingPlayer>;
}

export interface MissingPlayer {
  nbaid: number;
  name: string;
}

export interface PlayersResponse {
  missing: Array<Player>;
}

export interface Player {
  gid: number;
  name: string;
}
