export interface ContestTier {
  rank: number;
  payout: number;
}

export interface Contest {
  id: number;
  name: string;
  entries: number;
  tiers: Array<ContestTier>;
  days: Array<number>;
}

export interface ListContestsResponse {
  contests: Array<Contest>;
}
