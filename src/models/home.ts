export interface SeasonLineup {
  id: number;
  date: string;
  actual: number;
  expected: number;
  cost: number;
  cumulative: number;
  won: number;
}

export interface Season {
  season: number;
  count: number;
  percent: number;
  lineups: Array<SeasonLineup>;
}

export interface BalanceResponse {
  seasons: Array<Season>;
}
