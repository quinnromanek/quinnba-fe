import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/games/:date?",
    name: "Games",
    component: () =>
      import(/* webpackChunkName: "games" */ "../views/Games.vue"),
  },
  {
    path: "/players",
    name: "Players",
    component: () =>
      import(/* webpackChunkName: "players" */ "../views/Players.vue"),
  },
  {
    path: "/models",
    name: "Models",
    component: () =>
      import(/* webpackChunkName: "players" */ "../views/Models.vue"),
  },
  {
    path: "/contests/:id?",
    name: "Contests",
    component: () =>
      import(/* webpackChunkName: "players" */ "../views/Contests.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
