export default class Configuration {
  static get EnvConfig(): { [key: string]: string } {
    return {
      quinnbaUrl: "$VUE_APP_QUINNBA_API_URL",
      airflowUrl: "$VUE_APP_AIRFLOW_URL",
    };
  }

  static value(name: string): string {
    if (!(name in this.EnvConfig)) {
      return "";
    }

    const value = this.EnvConfig[name];

    if (!value) {
      console.log(`Configuration: Value for "${name}" is not defined`);
      return "";
    }

    if (value.startsWith("$VUE_APP_")) {
      // value was not replaced, it seems we are in development.
      // Remove $ and get current value from process.env
      const envName = value.substr(1);
      const envValue = process.env[envName];
      if (envValue) {
        return envValue;
      } else {
        console.log(
          `Configuration: Environment variable "${envName}" is not defined`,
        );
      }
    } else {
      // value was already replaced, it seems we are in production.
      return value;
    }
    return "";
  }
}
