import gql from "graphql-tag";

export const MISSING_PLAYERS_QUERY = gql`
  query MissingPlayers {
    missingPlayers {
      gid
      name
      nbaid
      fanduelId
    }
  }
`;
