module.exports = {
  transpileDependencies: ["vuetify"],
  devServer: {
    proxy: {
      "/airflow/api": {
        target: "http://localhost:8000",
        changeOrigin: true,
        pathRewrite: { "/airflow/api": "/api" },
      },
    },
  },
};
