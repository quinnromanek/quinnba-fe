# build stage
FROM node:14-buster-slim as build-stage
RUN apt update && apt install -y \
        python3 \
        make \
        g++
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# production stage
FROM nginx:stable-alpine as production-stage
RUN mkdir /app
WORKDIR /app
COPY ./entrypoint.sh /docker-entrypoint.d/entrypoint.sh
COPY --from=build-stage /app/dist /app
EXPOSE 80
COPY nginx.conf /etc/nginx/nginx.conf

